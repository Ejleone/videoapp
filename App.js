/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import configureStore from './src/Store';
import Root from './src/Navigation/Routes';
import { Provider } from 'react-redux';

store = configureStore();

export default class App extends Component<Props> {
	render() {
		return (
			<Provider store={store}>
				<Root />
			</Provider>
		);
	}
}
