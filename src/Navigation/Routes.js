import { StackNavigator, TabNavigator } from 'react-navigation';
import CallHistory from '../Containers/CallHistory';
import Login from '../Containers/Login';
import VideoScreen from '../Containers/VideoScreen';

const screens = TabNavigator(
	{
		CallHistory: { screen: CallHistory },
		VideoScreen: { screen: VideoScreen }
	},
	{
		tabBarPosition: 'top',
		animationEnabled: true,
		swipeEnabled: true,
		tabBarOptions: {
			showIcon: true,
			labelStyle: {
				fontSize: 14
			},
			style: {
				backgroundColor: 'black'
			},
			tabStyle: {
				height: 49
			},
			iconStyle: {
				flexGrow: 0,
				marginTop: 1.5
			}
		}
	}
);

const Root = StackNavigator(
	{
		Login: { screen: Login },
		screens: { screen: screens }
	},
	{
		headerMode: 'none'
	}
);

export default Root;
