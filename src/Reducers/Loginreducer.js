import { InitialState } from './InitialState';
import * as types from '../Actions/ActionTypes';

export const Loginreducer = (state = {}, action) => {
	switch (action.type) {
		case types.LOGIN_REQUEST:
			return {
				...state
			};

		case types.LOGIN_SUCCESS:
			return {
				...state
			};

		case types.LOGIN_FAILURE:
			return {
				...state
			};
		default:
			return state;
	}
};
