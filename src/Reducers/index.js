import { combineReducers } from 'redux';

import { Loginreducer } from './Loginreducer';
import { VideoListreducer } from './VideoListreducer';
import { CallHistoryreducer } from './CallHistoryreducer';

const rootReducer = combineReducers({
	Loginreducer,
	CallHistoryreducer,
	VideoListreducer
});

export default rootReducer;
