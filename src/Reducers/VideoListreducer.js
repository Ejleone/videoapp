import { InitialState } from './InitialState';
import * as types from '../Actions/ActionTypes';
export const VideoListreducer = (state = {}, action) => {
	switch (action.type) {
		case types.VIDEOH_REQUEST:
			return {
				...state
			};
		case types.VIDEOH_SUCCESS:
			return {
				...state
			};

		case types.VIDEOH_FAILURE:
			return {
				...state
			};

		default:
			return state;
	}
};
