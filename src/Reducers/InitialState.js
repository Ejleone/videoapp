export const InitialState = {
	user: {
		id: -1,
		name: '',
		email: '',
		password: '',
		access_token: ''
	},
	VideoList: [],
	CallHistory: []
};
