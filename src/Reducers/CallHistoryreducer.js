import { InitialState } from './InitialState';
import * as types from '../Actions/ActionTypes';

export const CallHistoryreducer = (state = {}, action) => {
	switch (action.type) {
		case types.CALLH_REQUEST:
			return {
				...state
			};

		case types.CALLH_SUCCESS:
			return {
				...state
			};

		case types.CALLH_FAILURE:
			return {
				...state
			};

		default:
			return state;
	}
};
