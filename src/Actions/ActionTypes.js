//Define Actions here

export const LOGIN_REQUEST = 'login-request';
export const LOGIN_SUCCESS = 'login-success';
export const LOGIN_FAILURE = 'login-failure';

export const CALLH_REQUEST = 'callh-request';
export const CALLH_SUCCESS = 'callh-success';
export const CALLH_FAILURE = 'callh-failure';

export const VIDEOH_REQUEST = 'videoh-request';
export const VIDEOH_SUCCESS = 'videoh-success';
export const VIDEOH_FAILURE = 'videoh-failure';
