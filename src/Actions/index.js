import Login from './LoginAction';
import fetchVideoList from './VideoListAction';
import fetchCallList from './CallHistoryAction';

export const ActionCreators = Object.assign({}, Login);
