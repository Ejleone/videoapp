import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../Actions';
import VideoList from './VideoList';

function mapStateToProps(state) {
	return {
		settings: state.settingsReducers
	};
}
function mapDispatchToProps(dispatch) {
	return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(VideoList);
