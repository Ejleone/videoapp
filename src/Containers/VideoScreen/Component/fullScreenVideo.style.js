import { StyleSheet } from 'react-native';
import config from './Config.js';
import { Dimensions } from 'react-native';
var screenHei = Dimensions.get('window').height;
var screenWid = Dimensions.get('window').width;

export default StyleSheet.create({
	container: {
		width: screenWid,
		height: screenHei
	},
	video: {
		width: screenWid,
		height: screenHei
	}
});
