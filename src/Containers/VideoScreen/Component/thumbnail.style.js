import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';
Dimensions.get('window').height;
var screenWid = Dimensions.get('window').width;
import config from './Config.js';

export default StyleSheet.create({
	container: {
		position: 'absolute',
		//borderWidth: 1, borderColor: "red",
		height: 100, // config.thumbnailHeight,
		width: screenWid,
		bottom: 0,
		left: 0
	},
	thumbnailContainer: {
		//borderWidth: 1, borderColor: "green"
		paddingLeft: 10
	},
	thumbnail: {
		width: 100,
		height: 100 //config.thumbnailHeight
	},
	activeThumbnail: {
		borderColor: '#CCC',
		borderWidth: 2
	}
});
