import { Dimensions } from 'react-native';
export const config = {
	screenWidth: window.width,
	screenHeight: window.height,
	thumbnailHeight: 100,
	useRCTView: true, //debug or not?
	video: {
		minWidth: 500,
		minHeight: 300,
		minFrameRate: 30
	}
};
