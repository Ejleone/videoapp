/* @flow */

import React, { Component } from 'react';
import {
	View,
	Text,
	ActivityIndicator,
	Image,
	ImageBackground,
	TextInput,
	TouchableOpacity
} from 'react-native';
import axios from 'axios';
import Icon from 'react-native-vector-icons/Feather';
import secrets from 'react-native-config';
import styles from './Login.style';

export default class Login extends Component {
	static navigationOptions = {
		headerMode: 'none'
	};

	constructor(props) {
		super(props);

		this.state = {
			username: '',
			Password: '',
			isloading: false
		};
	}

	_doLogin() {
		this.props.navigation.navigate('screens');
	}

	render() {
		if (this.state.isloading == true) {
			return (
				<View style={styles.container}>
					<ActivityIndicator
						style={styles.activityIndicator}
						animating={true}
						color={'#ce0a3e'}
						size={'large'}
					/>
				</View>
			);
		}
		return (
			<ImageBackground
				source={{
					uri: 'https://www.hdwallpapers.net/previews/black-panther-1164.jpg'
				}}
				style={styles.container}>
				<View style={styles.card}>
					<View style={styles.inputWrap}>
						<View style={styles.iconWrap}>
							<Icon name="user" backgroundColor="#ce0a3e" size={40} />
						</View>
						<TextInput
							placeholder="Email"
							placeholderTextColor="#000"
							keyboardType="email-address"
							style={styles.input}
							onChangeText={(text) => this.setState({ username: text })}
						/>
					</View>

					<View style={styles.inputWrap}>
						<View style={styles.iconWrap}>
							<Icon name="lock" backgroundColor="#ce0a3e" size={40} />
						</View>
						<TextInput
							placeholder="Password"
							placeholderTextColor="#000"
							secureTextEntry
							style={styles.input}
							onChangeText={(pass) => this.setState({ password: pass })}
						/>
					</View>

					<TouchableOpacity activeOpacity={0.5} onPress={() => this._doLogin()}>
						<View style={styles.button}>
							<Text style={styles.buttonText}>Sign In</Text>
						</View>
					</TouchableOpacity>
				</View>
			</ImageBackground>
		);
	}
}
/*
_doLogin() {
	Navigation.navigate('screens');
	this.setState({
		isloading: true
	});
	var _this = this;
	const Bodyparam = {
		email: this.state.username,
		password: this.state.password,
		strategy: 'local',
		access_token: 'string'
	};
	// console.warn('Clickeddd' + this.state.username + this.state.password + JSON.stringify(Bodyparam))

	axios
		.post(secrets.BASE_URL, Bodyparam, {
			'Content-Type': 'application/json'
		})
		.then(function(response) {
			//  console.warn(response)
			// return response.json()
			_this.setState({
				isloading: false
			});
			const Token = response.data.data.accessToken;
			// console.warn(Token)
			_this._tomainscreen(Token);
		})
		.catch(function(error) {
			//  console.warn(error.name + error.message)
			Alert.alert(error.name, error.message);
			_this.setState({
				isloading: false
			});
		});
}

<TouchableOpacity
		activeOpacity={0.5}
		onPress={() => this._toforgotPass()}>
		<View   style={styles.facebookbutton} >
			<Text style={styles.buttonText}> </Text>
		</View>
	</TouchableOpacity>*/
