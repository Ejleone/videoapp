import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default StyleSheet.create({
	container: {
		backgroundColor: '#fff',
		flex: 1
	},
	card: {
		backgroundColor: '#fff',
		height: 250,
		marginLeft: 20,
		marginRight: 20,
		marginTop: 400
	},
	mimg: {
		width: undefined,
		height: 400
	},
	markWrap: {
		flex: 1,
		paddingVertical: 30
	},
	mark: {
		width: null,
		height: null,
		flex: 1
	},
	background: {
		width,
		height
	},
	wrapper: {
		paddingVertical: 30
	},
	inputWrap: {
		flexDirection: 'row',
		marginVertical: 10,
		height: 40,
		borderBottomWidth: 1,
		borderBottomColor: '#CCC'
	},
	iconWrap: {
		paddingHorizontal: 7,
		alignItems: 'center',
		justifyContent: 'center'
	},
	icon: {
		height: 20,
		width: 20
	},
	input: {
		flex: 1,
		paddingHorizontal: 10
	},
	button: {
		backgroundColor: '#000000',
		paddingVertical: 20,
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 30
	},
	facebookbutton: {
		backgroundColor: '#ce0a3e',
		paddingVertical: 20,
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 30
	},
	buttonText: {
		color: '#FFF',
		fontSize: 18
	},
	forgotPasswordText: {
		color: '#D8D8D8',
		backgroundColor: 'transparent',
		textAlign: 'right',
		paddingRight: 15
	},
	signupWrap: {
		backgroundColor: 'transparent',
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center'
	},
	accountText: {
		color: '#D8D8D8'
	},
	signupLinkText: {
		color: '#FFF',
		marginLeft: 5
	},
	loader: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	activityIndicator: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		height: 80
	}
});
