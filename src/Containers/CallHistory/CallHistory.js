/* @flow */

import React, { Component } from 'react';
import { View, Text, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import styles from './CallHistory.style';

const dummyrecent = [
	{ id: 0, name: 'elijah' },
	{ id: 0, name: 'elijah' },
	{ id: 0, name: 'elijah' },
	{ id: 0, name: 'elijah' }
];
const extractKey = ({ id }) => id;

export default class CallHistory extends Component {
	static navigationOptions = {
		tabBarIcon: () => <Icon name="user" backgroundColor="#ce0a3e" size={40} />
	};

	constructor(props) {
		super(props);

		this.state = {
			isloading: false
		};
	}

	renderItem = ({ item }) => {
		return <Text style={styles.row}>{item.name}</Text>;
	};

	_LoadCallHistory() {
		this.setState({
			isloading: true
		});
		var _this = this;
		const Bodyparam = {
			email: this.state.username,
			password: this.state.password,
			strategy: 'local',
			access_token: 'string'
		};
		// console.warn('Clickeddd' + this.state.username + this.state.password + JSON.stringify(Bodyparam))

		axios
			.post(url, Bodyparam, {
				'Content-Type': 'application/json'
			})
			.then(function(response) {
				//  console.warn(response)
				// return response.json()
				_this.setState({
					isloading: false
				});
				const Token = response.data.data.accessToken;
				// console.warn(Token)
				_this._tomainscreen(Token);
			})
			.catch(function(error) {
				//  console.warn(error.name + error.message)
				Alert.alert(error.name, error.message);
				_this.setState({
					isloading: false
				});
			});
	}

	render() {
		if (this.state.isloading == true) {
			return (
				<ActivityIndicatorIOS
					animating={true}
					color={'#808080'}
					size={'small'}
				/>
			);
		}
		return (
			<View style={styles.container}>
				<Text> Recent Calls will appear here </Text>

				<FlatList
					data={dummyrecent}
					renderItem={this.renderItem}
					keyExtractor={extractKey}
				/>
			</View>
		);
	}
}
