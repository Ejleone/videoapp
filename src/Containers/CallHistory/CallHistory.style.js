import { StyleSheet } from 'react-native';
export default StyleSheet.create({
	container: {
		flex: 1
	},
	row: {
		padding: 15,
		marginBottom: 5,
		backgroundColor: 'skyblue'
	}
});
